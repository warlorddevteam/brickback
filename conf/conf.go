package conf

var ServerConf = Conf{"test", "localhost",
	"8080", "postgres",
	"393483", "localhost",
	"5432", "brick"}

type Conf struct {
	AppMod         string `json:"app_mod"`
	HTTPServerHost string `json:"http_server_host,omitempty"`
	HTTPServerPort string `json:"http_server_port,omitempty"`
	DbUser         string `json:"cocrouch_db_user,omitempty"`
	DbPassword     string `json:"cocrouch_db_password,omitempty"`
	DbHost         string `json:"cocrouch_db_host,omitempty"`
	DbPort         string `json:"cocrouch_db_port,omitempty"`
	DbName         string `json:"cocrouch_db_name,omitempty"`
}
