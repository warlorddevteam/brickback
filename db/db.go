package db

import (
	"bitbucket.org/renegatumsoulteame/brickback/conf"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

func GetDBConnection() (*gorm.DB, error) {
	connectionString := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v",
		conf.ServerConf.DbHost, conf.ServerConf.DbPort,
		conf.ServerConf.DbUser, conf.ServerConf.DbName, conf.ServerConf.DbPassword)
	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	return db, nil
}
