package log

import (
	"github.com/jinzhu/gorm"
	"time"
)

type ProductLog struct {
	gorm.Model
	Size       string
	Price      float64
	Producer   string
	Format     string
	Color      string
	ChangeDate *time.Time
}
