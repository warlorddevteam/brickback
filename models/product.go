package models

import (
	"bitbucket.org/renegatumsoulteame/brickback/db"
	"errors"
	"github.com/jinzhu/gorm"
)

type Product struct {
	gorm.Model
	Size       string
	Price      float64
	PromoPrice float64
	Producer   string
	Type       string
	Format     string
	Color      string
}

func (p Product) GetAllProducts() ([]Product, error) {
	conn, err := db.GetDBConnection()
	if err != nil {
		return []Product{}, errors.New("connection fall cant login user")
	}
	allProd := []Product{}
	conn.Find(&allProd)
	return allProd, nil
}

func (p Product) GetProductsByType(t string) ([]Product, error) {
	conn, err := db.GetDBConnection()
	if err != nil {
		return []Product{}, errors.New("connection fall cant login user")
	}
	allProd := []Product{}
	conn.Where("type = ?", t).Find(&allProd)
	if len(allProd) == 0 {
		return []Product{}, errors.New("is empty")
	}
	return allProd, nil
}

func (p Product) ProductByID(id int) (Product, error) {
	p := Product{}
	conn, err := db.GetDBConnection()
	if err != nil {
		return Product{}, errors.New("connection fall cant login user")
	}
	conn.Where("id = ?", id).First(&p)
	return p, nil
}
