package models

import (
	"bitbucket.org/renegatumsoulteame/brickback/db"
	"log"
)

func init() {
	dbconnection, err := db.GetDBConnection()
	if err != nil {
		log.Fatal(err)
	}
	dbconnection.AutoMigrate(Product{}, Cart{}, Order{}, User{}, Promo{})
}
