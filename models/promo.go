package models

import "github.com/jinzhu/gorm"

type Promo struct {
	gorm.Model
	UserID    int
	PromoCode string
}
