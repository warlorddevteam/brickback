package models

import "github.com/jinzhu/gorm"

type Order struct {
	gorm.Model
	UserId            int
	ProductID         int
	ProductAmount     float64
	ProductTotalPrice float64
}
