package models

import (
	"bitbucket.org/renegatumsoulteame/brickback/db"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type User struct {
	gorm.Model
	Token     string
	Name      string
	SureName  string
	LastName  string
	INN       string
	Email     string
	PromoCode string
	Role      string
	Password  string
}

func (u User) Login() (string, error) {
	conn, err := db.GetDBConnection()
	if err != nil {
		return uuid.UUID{}.String(), errors.New("connection fall cant login user")
	}
	isValid := u.findUserAndCheck()
	if isValid {
		u.Token = uuid.Must(uuid.NewV4()).String()
		conn.Update(u)
		return u.Token, nil
	}
	return uuid.UUID{}.String(), errors.New("user not exist")
}

func (u User) Logout() {
	conn, _ := db.GetDBConnection()
	u.Token = uuid.UUID{}.String()
	conn.Update(User{})
}

func (u User) Registration() bool {
	user := User{}
	conn, err := db.GetDBConnection()
	if err != nil {
		return false
	}
	conn.First(&user, "inn = ?", u.INN)
	if user.INN != u.INN {
		conn.Create(u)
		return true
	}
	return false
}

func (u User) findUserAndCheck() bool {
	user := User{}
	conn, err := db.GetDBConnection()
	if err != nil {
		return false
	}
	conn.First(&user, "inn = ?", u.INN)
	if user.INN == u.INN {
		if user.Password == u.Password {
			return true
		}
		return false
	}
	return false
}
