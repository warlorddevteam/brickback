package controllers

import (
	"bitbucket.org/renegatumsoulteame/brickback/models"
	"github.com/labstack/echo"
	"net/http"
)

func GetAllPropucts(c echo.Context) error {
	products, err := models.Product{}.GetAllProducts()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, products)
}

func GetProductsByType(c echo.Context) error {
	var res struct {
		productType string `json:"product_type"`
	}
	c.Bind(res)
	products, err := models.Product{}.GetProductsByType(res.productType)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, products)
}

func GetProductByID(c echo.Context) error {
	var res struct {
		productID int `json:"product_id"`
	}
	c.Bind(res)
	product, err := models.Product{}.ProductByID(res.productID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, product)
}

func AddToCart(c echo.Context) error {

}

func DeleteCart(c echo.Context) error {

}
