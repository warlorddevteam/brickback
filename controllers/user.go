package controllers

import (
	"bitbucket.org/renegatumsoulteame/brickback/models"
	"github.com/labstack/echo"
	"net/http"
)

func GetUserRoleByToken() {

}

func Login(c echo.Context) error {
	// User ID from path `users/:id`
	u := new(models.User)
	c.Bind(u)
	uuid, err := u.Login()
	if err != nil {
		return c.JSON(http.StatusOK, uuid)
	}
	return c.JSON(http.StatusOK, uuid)
}

func Logout(c echo.Context) error {
	u := new(models.User)
	c.Bind(u)
	u.Logout()
	return c.JSON(http.StatusOK, "logout")
}

func Registration(c echo.Context) error {
	u := new(models.User)
	c.Bind(u)
	reg := u.Registration()
	if reg {
		return c.JSON(http.StatusOK, "registred")
	}
	return c.JSON(http.StatusOK, "error")
}
